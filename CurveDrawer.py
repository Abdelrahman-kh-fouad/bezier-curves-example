import numpy
import math
import cv2
from BezierCurve import BezierCurve

DEFAULT_POINT_COLOR = (255, 255, 255)
DEFAULT_CONTROL_POINTS = [(0, 0), (200, 200), (400, 0), (600, 200)]
CONTROL_POINTS_COLOR = (0, 0, 255)
SELECTED_CONTROL_POINT_COLOR = (0, 255, 0)


class CurveDrawer:

    def __init__(self, curve=None, width=800, height=500, point_width=1,
                 point_height=1, precision=1000, control_points_width=10, control_points_height=10,
                 x_control_points_step=5, y_control_points_step=5, x_move_view_step=20, y_move_view_step=20,
                 addons=None, delay=1):

        if curve is None:
            curve = BezierCurve(DEFAULT_CONTROL_POINTS)
        if addons is None:
            addons = []

        self.curve = curve
        self.image = None
        self.width = width
        self.height = height
        self.precision = precision
        self.point_width = point_width
        self.point_height = point_height
        self.control_points_width = control_points_width
        self.control_points_height = control_points_height
        self.selected_control_point = 0
        self.y_move_view_step = y_move_view_step
        self.x_move_view_step = x_move_view_step
        self.y_control_points_step = y_control_points_step
        self.x_control_points_step = x_control_points_step
        self.addons = addons
        self.current_t = 0
        self.delay = delay
        self.corner_x = -width // 2
        self.corner_y = -height // 2
        self.in_move_view_mode = False

    def draw_point(self, x, y, width=None, height=None, color=None):

        x -= self.corner_x
        y -= self.corner_y

        if x < 0 or y < 0 or x >= self.width or y >= self.height:
            return

        if width is None:
            width = self.point_width
        if height is None:
            height = self.point_height
        if color is None:
            color = DEFAULT_POINT_COLOR

        x1 = math.ceil(x)
        y1 = math.ceil(y)
        x2 = x1 + width
        y2 = y1 + height

        self.image[y1:y2, x1:x2] = color

    def get_line_y(self, old_y, slope, dx):
        return old_y + slope * dx

    def draw_horizontal_line(self, x1, x2, y, height=5, color=None):
        x1 = math.ceil(x1)
        x2 = math.ceil(x2)
        y = math.ceil(y)
        min_x = min(x1, x2)
        max_x = max(x1, x2)
        for x in range(min_x, max_x + 1):
            self.draw_point(x, y, width=1, height=height, color=color)

    def draw_vertical_line(self, x, y1, y2, width=5, color=None):
        x = math.ceil(x)
        y1 = math.ceil(y1)
        y2 = math.ceil(y2)
        min_y = min(y1, y2)
        max_y = max(y1, y2)
        for y in range(min_y, max_y + 1):
            self.draw_point(x, y, width=width, height=1, color=color)

    def draw_line_point_slope(self, point, slope=1, points_per_line=200, dx=0.1, point_width=5, point_height=5, color=None):

        x1 = point[0]
        y1 = point[1]
        x_offset = points_per_line * dx
        x2 = x1 + x_offset
        y2 = y1 + x_offset * slope
        return self.draw_line_two_points(
            (x1, y1),
            (x2, y2),
            dx=dx,
            point_width=point_width,
            point_height=point_height,
            color=color
        )

    def draw_line_two_points(self, point1, point2, dx=0.1, point_width=5, point_height=5, color=None):

        x1 = point1[0]
        x2 = point2[0]
        y1 = point1[1]
        y2 = point2[1]

        if x1 == x2:
            return self.draw_vertical_line(x1, x2, y1, width=point_width, color=color)
        elif y1 == y2:
            return self.draw_horizontal_line(x1, y1, y2, height=point_height, color=color)

        slope = (point1[0] - point2[0]) / (point1[1] - point2[1])

        min_x_point = point1 if point1[0] < point2[0] else point2
        max_x_point = point1 if point1[0] > point2[0] else point2

        x = min_x_point[0]
        y = min_x_point[1]
        target_x = max_x_point[0]

        while x <= target_x:
            self.draw_point(x, y, width=point_width, height=point_height, color=color)
            x += dx
            y += slope * dx

    def draw_line_centered_at_point(self, center_point, slope=1, points_per_line=200, dx=0.1, point_width=5, point_height=5, color=None):
        x = center_point[0] - (points_per_line / 2) * dx
        y = center_point[1] - self.get_line_y(center_point[0], slope, x - center_point[0])

        self.draw_line_point_slope(
            (x, y),
            slope=slope,
            points_per_line=points_per_line,
            dx=dx,
            point_width=point_width,
            point_height=point_height,
            color=color
        )

    def get_top_y(self):
        return self.corner_y

    def get_bottom_y(self):
        return self.get_top_y() + self.height - 1

    def get_left_x(self):
        return self.corner_x

    def get_right_x(self):
        return self.get_left_x() + self.width - 1

    def draw_axis(self, thickness=1, color=None):
        self.draw_horizontal_line(self.get_left_x(), self.get_right_x(), 0, height=thickness, color=color)
        self.draw_vertical_line(0, self.get_top_y(), self.get_bottom_y(), width=thickness, color=color)

    def draw(self):
        self.image = numpy.zeros((self.height, self.width, 3), numpy.uint8)

        self.draw_axis()

        for i in range(1, self.precision):
            point = self.curve.calculate_point_at(i / self.precision)
            self.draw_point(point[0], point[1], self.point_width, self.point_height)

        self.highlight_control_points()

    def notify_addons(self):
        for addon in self.addons:
            addon.set_t(self.current_t)
            addon.draw(self)

        self.current_t += 1 / self.precision
        if self.current_t > 1:
            self.current_t = 0

    def run(self):

        while True:

            self.draw()
            self.notify_addons()

            cv2.imshow('Curve', self.image)
            key = cv2.waitKey(self.delay)

            if key == ord('q'):
                break
            elif key != -1:
                self.handle_key(key)

        cv2.destroyAllWindows()

    def highlight_control_points(self):
        control_points = self.curve.control_points
        for i in range(len(control_points)):
            x = control_points[i][0]
            y = control_points[i][1]
            self.draw_point(
                x,
                y,
                self.control_points_width,
                self.control_points_height,
                CONTROL_POINTS_COLOR if i != self.selected_control_point else SELECTED_CONTROL_POINT_COLOR
            )

    def move_selected_control_point(self, dx, dy):
        point = self.curve.control_points[self.selected_control_point]
        x = point[0] + dx
        y = point[1] + dy
        self.curve.set_control_point(self.selected_control_point, (x, y))

    def move_view(self, dx, dy):
        self.corner_x += dx
        self.corner_y += dy

    def move(self, dx, dy):
        if self.in_move_view_mode:
            self.move_view(dx, dy)
        else:
            self.move_selected_control_point(dx, dy)

    def handle_key(self, key):

        key = chr(key)
        control_points = self.curve.control_points
        n = len(control_points)

        if key == 'd':
            self.selected_control_point = (self.selected_control_point + 1) % n
        elif key == 's':
            self.selected_control_point = (self.selected_control_point - 1 + n) % n
        elif key == 'h':
            self.move(-self.x_control_points_step, 0)
        elif key == 'l':
            self.move(self.x_control_points_step, 0)
        elif key == 'k':
            self.move(0, -self.y_control_points_step)
        elif key == 'j':
            self.move(0, self.y_control_points_step)
        elif key == 'v':
            self.in_move_view_mode = not self.in_move_view_mode

